var webpack = require('webpack')
var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')

const APP_DIR = path.resolve(__dirname, './src');
const DIST_DIR = path.resolve(__dirname, './public');

module.exports = {
  entry: APP_DIR + '/index.js',
  output: {
    path: DIST_DIR,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      { test: /\.js$/, use: 'babel-loader', exclude: 'node_modules' },
      { test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader'] }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: APP_DIR + '/template.html',
      filename: 'index.html',
      inject: 'body'
    })
  ]
}
